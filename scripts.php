<script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<script src="/js/libs.min.js"></script>
<script src="/js/main.min.js"></script>

<?php
// add Google Anlaytics if we are on the production site
if ($_SERVER['HTTP_HOST']==="XXXXXXX.com" || $_SERVER['HTTP_HOST']==="XXXXXXX.com") { 
    echo "<script>
            window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
            ga('create','UA-XXXXXXXXX-Y','auto');ga('send','pageview')
          </script>
          <script src='https://www.google-analytics.com/analytics.js' async defer></script>";
} ?>