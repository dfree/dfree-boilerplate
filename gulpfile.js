// Include gulp
const gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename');

// Include Our Plugins
const sass = require('gulp-sass'),
    minifycss = require('gulp-clean-css'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    notify = require("gulp-notify");

// Set up compression, prefixing, sourcemaps and destination
gulp.task('sass', function(){
  gulp.src(['src/scss/**/*.scss'])
    .pipe(plumber({errorHandler: notify.onError("Oh Snap!: <%= error.message %>")}))
    .pipe(sass())
    .pipe(autoprefixer('last 2 versions'))
    .pipe(minifycss())
    .pipe(gulp.dest('css/'))
});

gulp.task('libs', function(){
  return gulp.src('src/js/libs/*.js')
        .pipe(plumber({errorHandler: notify.onError("Oh Snap!: <%= error.message %>")}))
        .pipe(concat('libs.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('js/'))
});

gulp.task('scripts', function(){
  return gulp.src('src/js/*.js')
    .pipe(plumber({errorHandler: notify.onError("Oh Snap!: <%= error.message %>")}))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('js/'))
});

gulp.task('imgs', () =>
    gulp.src('src/img/**')
        .pipe(imagemin())
        .pipe(gulp.dest('images/'))
);

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('src/scss/**/*.scss', ['sass']),
    gulp.watch('src/js/*.js', ['scripts']),
    gulp.watch('src/js/libs/*.js', ['libs']),
    gulp.watch('src/img/**', ['imgs']);
});

// Default Task
gulp.task('default', ['sass', 'libs', 'scripts', 'imgs', 'watch']);